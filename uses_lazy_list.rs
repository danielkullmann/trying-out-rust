extern mod lazy_list;
use lazy_list::{LazyList,Empty,head};

fn main() {
  let result: Result<int, ()> = do task::try {
    let _: LazyList<int> = head(&Empty);
    1
  };
  assert!(result.is_err()); 
  assert!(head(&LazyList(1, || Empty)) == 1);
  io::println("OK");
}

