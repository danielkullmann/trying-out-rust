fn f(s: ~str) {
  io::println(s);
}

fn main() {
  f(copy os::args()[0]);
}
