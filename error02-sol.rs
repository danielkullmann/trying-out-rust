fn main() {
  let mut stack : ~[uint] = ~[];
  stack.push(1);
  let a = stack.pop();
  stack.push(a+1);
  assert!(stack.pop() == 2);
}
