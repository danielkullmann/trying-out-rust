#[link(name = "lazy_list", vers = "0.1", author = "Daniel Kullmann")];
#[crate_type = "lib"];

use core::cmp::Eq;
use core::vec::with_capacity;

enum LazyList<T> {
  Empty,
  LazyList(T, @fn() -> LazyList<T>)
}

pub fn head<T: Copy>(ll: &LazyList<T>) -> T {
  match *ll {
    Empty           => fail!(~"head of empty list"),
    LazyList(hd, _) => copy hd
  }
}

fn tail<T: Copy>(ll: &LazyList<T>) -> LazyList<T> {
  match *ll {
    Empty           => fail!(~"tail of empty list"),
    LazyList(_, tl) => tl()
  }
}

fn copy_list<T: Copy>(ll: &LazyList<T>) -> LazyList<T> {
  match *ll {
    Empty => Empty,
    LazyList(hd, tl) => LazyList(copy hd, || copy_list(&tl()))
  }
}


//#[doc="submodule ll for functions that return lazy lists. compare with submodule vec which returns vectors"] 
//mod ll {
//  use LazyList, Empty;
  pub fn ll_take<T: Copy>(ll: &LazyList<T>, count: uint) -> LazyList<T> {
    if count == 0 {
      Empty
    } else {
      match *ll {
        Empty => Empty,
        LazyList(hd, tl) => LazyList(hd, || ll_take(@(tl()), count-1))
      }
    }
  }
//}

//#[doc="submodule vec for functions that return vectors. compare with submodule ll which returns lazy lists"] 
//mod vec {
//  use LazyList, head, tail;
//  use core::vec::with_capacity;
  pub fn vec_take<T: Copy+Owned>(ll: @LazyList<T>, count: uint) -> ~[T] {
    let mut vec: ~[T] = with_capacity(count);
    let mut l = ll;
    let mut c = count;
    while c > 0 {
      vec.push(head(l)); 
      l = @tail(l);
      c -= 1;
    }
    return vec;
  }
//}


impl <T: Eq+Copy> Eq for LazyList<T> {
    fn eq(&self, other: &LazyList<T>) -> bool {
      match self {
        &Empty => match other {
          &Empty => true,
          _      => false
        },
        &LazyList(h1, t1) => match other {
          &LazyList(h2, t2) => h1 == h2 && t1() == t2(),
          _                     => false
        }
      }
    }
    fn ne(&self, other: &LazyList<T>) -> bool { !self.eq(other) }
}

pub fn count<N: Add<N,N>+Copy+Owned>(start: N, step: N) -> LazyList<N> {
  return LazyList(copy start, || count(start + step, copy step));
}

fn iterate<T: Copy+Owned>(start: T, f: @fn(T) -> T) -> LazyList<T> {
  return LazyList(start, || iterate(f(start), f));
}

fn repeat<T: Copy+Owned>(value: T) -> LazyList<T> {
  return LazyList(value, || repeat(value));
}

//fn cycle<T: Copy>(values: &LazyList<T>) -> LazyList<T> {
//  let vs = copy_list(values);
//  fn helper<T: Copy>(current: @LazyList<T>, values: @LazyList<T>) -> LazyList<T> {
//    match *current {
//      Empty => LazyList( head(values), || helper(@tail(values), values)),
//      _ => return LazyList( head(current), || helper(@tail(current), values))
//    }
//  }
//  return helper(@vs, @vs);
//}

//fn cycle_vec<T: Copy>(values: @[T]) -> LazyList<T> {
//  fn helper<T: Copy>(values: @[T], n: uint, i: uint) -> LazyList<T> {
//    return LazyList( values[i%n], || helper(values, n, i+1));
//  }
//  let n = values.len();
//  return helper(values, n, 0);
//}

//fn zip<T: Copy Owned, U: Copy Owned>(a: &LazyList<T>, b: &LazyList<U>) -> LazyList<(T,U)> {
//  match (*a, *b) {
//    (Empty, Empty) => Empty,
//    _ => LazyList( (head(a), head(b)), || zip(&tail(a), &tail(b)))
//  }
//}

fn numbers(start: uint) -> LazyList<uint> {
  return LazyList(start, || numbers(start + 1));
}

/////////////////////////////////////////////////////////////////////////

#[test]
fn test_head() {
  let result: Result<int, ()> = do task::try {
    let ll: @LazyList<int>  = @Empty;
    let _: int = head(ll);
    1
  };
  assert!(result.is_err()); 

  let ll: LazyList<int> = LazyList(1, || Empty);
  let hd: int = head(@ll);
  assert!(hd == 1);
}

#[test]
fn test_tail() {
  let result: Result<int, ()> = do task::try {
    let ll: @LazyList<int> = @Empty;
    let _: LazyList<int> = tail(ll);
    1
  };
  assert!(result.is_err()); 

  let ll: @LazyList<int> = @LazyList(1, || Empty);
  let tl: LazyList<int> = tail(ll);

  //assert!(tl == @Empty);
  // above line does not work yet (no Eq impl), so I'll use the next line instead
  assert!(match tl {
    Empty => true,
    _ => false
  });
}

#[test]
fn test_vec_take() {
  let ll: LazyList<int> = LazyList(1, || LazyList(2, || LazyList(2, || LazyList(3, || Empty) ) ) );
  assert!(vec_take(@ll, 2) == ~[1,2]);
}

#[test]
fn test_ll_take() {
  let ll: LazyList<int> = LazyList(1, || LazyList(2, || LazyList(2, || LazyList(3, || Empty) ) ) );
  assert!(head(&ll_take(@ll, 2)) == 1);
  assert!(head(&tail(&ll_take(@ll, 2))) == 2);
  assert!(match tail(&tail(&ll_take(@ll, 2))) {
    Empty => true,
    _     => false
  });
}

#[test]
fn test_numbers() {
  let ll = numbers(1);
  assert!(head(&ll) == 1);
  assert!(head(&tail(&ll)) == 2);
}

#[test]
fn test_eq() {
  assert!(Empty::<int> == Empty::<int>);
  assert!(LazyList(1, || LazyList(2, || LazyList(3, || Empty))) != Empty);
  assert!(LazyList(1, || LazyList(2, || LazyList(3, || Empty))) == LazyList(1, || LazyList(2, || LazyList(3, || Empty))));
}

#[test]
fn test_iterate() {
  let plus3: @fn(int) -> int = |n| n+3;
  assert!(head(&iterate(2, plus3)) == 2);
  assert!(head(&tail(&iterate(2, plus3))) == 5);
  assert!(head(&tail(&tail(&iterate(2, plus3)))) == 8);
}

#[test]
fn test_repeat() {
  assert!(head(&repeat(2)) == 2);
  assert!(vec_take(@repeat(2), 10) == ~[2,2,2,2,2,2,2,2,2,2]);
}

#[test]
fn test_copy() {
  assert!(copy_list(&LazyList(1, || LazyList(2, || LazyList(3, || Empty)))) == LazyList(1, || LazyList(2, || LazyList(3, || Empty))));
}

