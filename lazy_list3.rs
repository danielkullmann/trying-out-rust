#[link(name = "lazy_list3", vers = "0.1", author = "Daniel Kullmann")];
#[crate_type = "lib"];

/**
*/

extern mod std;
use core::cmp::Eq;

struct LazyList<T> {
  /** Port that provides values */
  value: comm::Port<T>,
  /** Chan to ask for the next value.
   * This is needed because otherwise, the task providing the values would be
   * busy producing infinite values.
   */
  next: comm::Chan<bool>
}

impl <T: Owned> LazyList<T> {
  fn head(&self) -> T {
    self.next.send(true);
    match self.value.try_recv() {
      Some(v) => return v,
      None => fail!(~"head of empty list")
    }
  }

  fn headOr(&self, val: T) -> T {
    self.next.send(true);
    match self.value.try_recv() {
      Some(v) => return v,
      None => return val
    }
  }

  fn headOption(&self) -> Option<T> {
    self.next.send(true);
    return self.value.try_recv();
  }

  fn stop(&self) {
    self.next.send(false);
  }

}

fn from_vec<T: Copy+Owned>(vec: ~[T]) -> LazyList<T> {
  let (vport, vchan): (comm::Port<T>, comm::Chan<T>) = comm::stream();
  let (nport, nchan): (comm::Port<bool>, comm::Chan<bool>) = comm::stream();
  do task::spawn || { 
    // move vchan, move nport, move vec
    for vec.each() |val| {
      match nport.recv() { 
        false => break,
        true => ()
      }
      vchan.send(*val);
    }
  }
  return LazyList{value: vport, next: nchan};
}

fn zip<T: Owned+Eq, U: Owned+Eq>(l1: LazyList<T>, l2: LazyList<U>) -> LazyList<(T,U)> {
  let (vport, vchan): (comm::Port<(T,U)>, comm::Chan<(T,U)>) = comm::stream();
  let (nport, nchan): (comm::Port<bool>, comm::Chan<bool>) = comm::stream();
  do task::spawn || { 
    // move vchan, move nport, move l1, move l2
    loop {
      l1.next.send(true);
      l2.next.send(true);
      let v1 = l1.value.try_recv();
      let v2 = l2.value.try_recv();
      if v1 == None || v2 == None {
        break;
      }
      match nport.try_recv() { 
        None => break,
        Some(false) => break,
        Some(true) => ()
      }
      vchan.send((v1.unwrap(),v2.unwrap()));
    }
    l1.next.send(false);
    l2.next.send(false);
  }
  return LazyList{value: vport, next: nchan};
}

fn numbers(start: uint) -> LazyList<uint> {
  let (vport, vchan): (comm::Port<uint>, comm::Chan<uint>) = comm::stream();
  let (nport, nchan): (comm::Port<bool>, comm::Chan<bool>) = comm::stream();
  do task::spawn || { 
    // move vchan, move nport
    let mut number = copy start;
    loop {
      match nport.try_recv() { 
        None => break,
        Some(false) => break,
        Some(true) => ()
      }
      vchan.send(number);
      number += 1;
    }
  }
  return LazyList{value: vport, next: nchan};
}

fn take<T: Owned>(l: ~LazyList<T>, n: uint) -> LazyList<T> {
  let (vport, vchan): (comm::Port<T>, comm::Chan<T>) = comm::stream();
  let (nport, nchan): (comm::Port<bool>, comm::Chan<bool>) = comm::stream();
  do task::spawn || { 
    // move vchan, move nport, move l
    let mut n = copy n;
    while n > 0 {
      l.next.send(true);
      let value = l.value.try_recv();
      if value.is_none() { break; }
      match nport.try_recv() { 
        None => break,
        Some(false) => break,
        Some(true) => ()
      }
      vchan.send(value.unwrap());
      n -= 1;
    }
  }
  return LazyList{value: vport, next: nchan};
}

fn take_vec<T: Owned>(l: &LazyList<T>, n: uint) -> ~[T] {
  let mut vec: ~[T] = ~[];
  let mut n = copy n;
  while n > 0 {
    l.next.send(true);
    let value = l.value.try_recv();
    match value {
      Some(v) => vec.push(v),
      None => break
    }
    n -= 1;
  }
  l.next.send(false);
  return vec;
}

fn dup<T: Copy+Owned>(l: ~LazyList<T>) -> (LazyList<T>, LazyList<T>) {
  let (v1port, v1chan): (comm::Port<T>, comm::Chan<T>) = comm::stream();
  let (v2port, v2chan): (comm::Port<T>, comm::Chan<T>) = comm::stream();
  let (n1port, n1chan): (comm::Port<bool>, comm::Chan<bool>) = comm::stream();
  let (n2port, n2chan): (comm::Port<bool>, comm::Chan<bool>) = comm::stream();
  do task::spawn || {
    // move l, move v1chan, move v2chan, move n1port, move n2port
    let mut stop = 0;
    loop {
      let b = comm::select2i(&n1port, &n2port);
      l.next.send(true);
      let v = l.value.recv();
      v1chan.send(copy v);
      v2chan.send(copy v);
      match b {
        Left(()) => match n1port.recv() {
          false => stop = stop | 1,
          true  => ()
        },
        Right(())  => match n2port.recv() {
          false => stop = stop | 2,
          true  => ()
        }
      }
      if stop == 3 {
        break;
      }
    }
    l.stop();
  }

  let l1 = LazyList{value: v1port, next: n1chan};
  let l2 = LazyList{value: v2port, next: n2chan};
  (l1, l2)
}


#[test]
fn test_numbers() {
  let ns = numbers(0);
  assert!(ns.head() == 0);
  assert!(ns.head() == 1);
  assert!(ns.head() == 2);
  assert!(ns.head() == 3);
  ns.stop();
}

#[test]
fn test_take() {
  let ns = take(~numbers(0), 4);
  assert!(ns.head() == 0);
  assert!(ns.head() == 1);
  assert!(ns.head() == 2);
  assert!(ns.head() == 3);
  assert!(ns.headOption() == None);
  ns.stop();
}

#[test]
fn test_take_vec() {
  let ns = numbers(0);
  assert!(take_vec(&ns, 4) == ~[0,1,2,3]);
  ns.stop();
}

#[test]
fn test_from_vec() {
  assert!(take_vec(&from_vec(~[0,1,2,3]),4) == ~[0,1,2,3]);
  assert!(take_vec(&from_vec(~[0,1,2,3]),6) == ~[0,1,2,3]);
}

#[test]
fn test_dup() {
  let ns = ~numbers(0);
  let (a,b) = dup(ns);
  io::println(fmt!("a: %?", take_vec(&a,4)));
  io::println(fmt!("b: %?", take_vec(&b,4)));
  //assert!(take_vec(a,4) == ~[0,1,2,3]);
  //assert!(take_vec(b,4) == ~[0,1,2,3]);
  a.stop();
  b.stop();
}

