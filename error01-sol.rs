fn main() {
  let mut a : uint = 0;
  let no : &fn() = || {
    a = 1;
  };
  no();
  io::println(a.to_str());
}

