fn main() {
  let mut number = 600851475143;
  let mut factors : ~[(uint,uint)] = ~[];
  let mut primes : ~[uint] = ~[];
  let max = f64::sqrt(number as f64) as uint + 1;
  for uint::range(2, max) |n| {
    let isprime = vec::all(primes, |m| n%*m != 0 );
    if isprime {
      primes.push(n);
      let mut k = 0;
      while number % n == 0 {
        number = number / n;
        k += 1;
      }
      if k>0 {
        factors.push((n, k));
      }
      if number == 1 { break; }
    }
  };
  io::println(factors[factors.len()-1].to_str());
}
