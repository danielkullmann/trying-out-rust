#[link(name = "linked_list", vers = "0.1", author = "Daniel Kullmann")];
#[crate_type = "lib"];

use core::cmp::Eq;

enum LinkedList<T> {
  Empty,
  LinkedList(T, @LinkedList<T>)
}

fn head<T: Copy>(ll: @LinkedList<T>) -> T {
  match ll {
    @Empty => fail!(~"head of empty list"),
    @LinkedList(hd, _) => copy hd
  }
}

fn tail<T: Copy>(ll: @LinkedList<T>) -> @LinkedList<T> {
  match ll {
    @Empty => fail!(~"tail of empty list"),
    @LinkedList(_, tl) => tl
  }
}

fn count<T: Add<T,T> + Ord + Copy>(start: T, end: T, step: T) -> @LinkedList<T> {
  if start > end {
    @Empty
  } else {
    @LinkedList(start, count(start+step, end, step))
  }
}

impl <T: Eq> Eq for LinkedList<T> {
    fn eq(&self, other: &LinkedList<T>) -> bool {
      match self {
        &Empty => match other {
          &Empty => true,
          _     => false
        },
        &LinkedList(ref h1, t1) => match other {
          &LinkedList(ref h2, t2) => h1 == h2 && t1 == t2,
          _                => false
        }
      }
    }
    fn ne(&self, other: &LinkedList<T>) -> bool { !self.eq(other) }
}

//mod itertools {
//  use LinkedList;
//  pub fn count<N: Num>(start: r/N, step: r/N) -> LinkedList<N> {
//    let strt = copy start;
//    let stp = copy step;
//    return LinkedList(strt, || &count( strt + stp, stp) );
//  }
//}

#[test]
fn test_head() {
  let result: Result<int, ()> = do task::try {
    let ll: @LinkedList<int>  = @Empty;
    let _: int = head(ll);
    1
  };
  assert!(result.is_err()); 

  let ll: LinkedList<int> = LinkedList(1, @Empty);
  let hd: int = head(@ll);
  assert!(hd == 1);
}

#[test]
fn test_tail() {
  let result: Result<int, ()> = do task::try {
    let ll: @LinkedList<int> = @Empty;
    let _: @LinkedList<int> = tail(ll);
    1
  };
  assert!(result.is_err()); 

  let ll: @LinkedList<int> = @LinkedList(1,  @Empty);
  let tl: @LinkedList<int> = tail(ll);

  assert!(tl == @Empty);
  // above line does not work yet (no Eq impl), so I'll use the next line instead
  assert!(match tl {
    @Empty => true,
    _ => false
  });
}

#[test]
fn test_count() {
  assert!(count(6, 1, 1) == @Empty);
  assert!(count(1, 6, 1) == @LinkedList(1, @LinkedList(2, @LinkedList(3, @LinkedList(4, @LinkedList(5, @LinkedList(6, @Empty)))))));
  assert!(count(1, 18, 3) == @LinkedList(1, @LinkedList(4, @LinkedList(7, @LinkedList(10, @LinkedList(13, @LinkedList(16, @Empty)))))));
}

