extern mod vec_util;

fn main() {
  // imperative
  let mut sum : uint = 0;
  for uint::range(1,1000)  |x: uint| { if x%3==0 || x%5==0 {sum += x} }
  io::println(sum.to_str());

  //functional
  let add = |a: uint,b: &uint| a+*b;
  io::println(vec::foldl(0, vec::filter(vec_util::count(1u,999u,1u), |n| *n%3==0 || *n%5==0 ), add).to_str());

}
