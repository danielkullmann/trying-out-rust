#[link(name = "lazy_list2", vers = "0.1", author = "Daniel Kullmann")];
#[crate_type = "lib"];

use core::cmp::Eq;

trait LazyList<T: Copy> {
  fn empty(&self) -> bool;
  fn head(&self) -> T;
  fn tail(&self) -> LazyList<T>;
}

enum Empty<T>{
  Empty()
}

struct Item<T: Copy> {
  hd: T,
  tl: @fn() -> LazyList<T>
}

impl <T: Copy> LazyList<T> for Empty<T> {
  fn empty(&self) -> bool { true }
  fn head(&self) -> T { fail!(~"head of empty list"); }
  fn tail(&self) -> LazyList<T> { fail!(~"tailof empty list"); }
}

impl <T: Copy> LazyList<T> for Item<T> {
  fn empty(&self) -> bool { false }
  fn head(&self) -> T { self.hd }
  fn tail(&self) -> LazyList<T> { (self.tl)() }
}

fn empty<T: Copy>() -> LazyList<T>{
  Empty::<T> as (LazyList<T>)
}

fn item<T: Copy>(head: T, tail: @fn() -> LazyList<T>) -> LazyList<T> {
  Item{hd: head, tl: tail} as (LazyList<T>)
}

fn numbers(start: uint) -> LazyList<uint> {
  return Item{hd: start, tl: || numbers(start + 1)} as (LazyList<uint>);
}

fn copy_list<T: Copy>(ll: @LazyList<T>) -> @LazyList<T> {
  if ll.empty() {
    return Empty::<T> as (LazyList<T>);
  } else {
    return Item{ hd: ll.head(), tl: || ll.tail() } as (LazyList<T>);
  }
}


impl <T: Eq+Copy> Eq for @LazyList<T> {
  fn eq(&self, other: &@LazyList<T>) -> bool {
    if self.empty() && other.empty() { return true };
    if self.empty() || other.empty() { return false };
    if self.head() != other.head()   { return false };
    return self.tail() == other.tail();
  }
  fn ne(&self, other: &@LazyList<T>) -> bool { !self.eq(other) }
}

//pub fn count<N: Num Copy Owned>(start: N, step: N) -> LazyList<N> {
//  return LazyList(copy start, || count(start + step, copy step));
//}


#[test]
fn test_numbers() {
  let ll = numbers(1);
  assert!(ll.head() == 1);
  assert!(ll.tail().head() == 2);
}

#[test]
fn test_copy() {
  let cp = copy_list(item(1, || item(2, empty::<uint>)));
  assert!(cp.head() == 1);
  assert!(cp.tail().head() == 2);
  assert!(cp.tail().tail().empty());
  assert!(cp == copy_list(item(1, || item(2, empty::<uint>))));
}

