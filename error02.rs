fn main() {
  let mut stack : ~[uint] = ~[];
  stack.push(1);
  stack.push(stack.pop()+1);
  assert!(stack.pop() == 2);
}
