PHONY: liblazy_list.so libvec_util.so clean rust-git rust-0.5

all:
	@echo Please select a target

clean:
	rm -f lib*.so lazy_list lazy_list2 lazy_list3 linked_list uses_lazy_list vec_util euler-001 euler-002 euler-003 error01 error02 error03 error01-sol error02-sol error03-sol error04 hackvm protocol01 

euler-001: libvec_util.so
euler-002:
euler-003:
error01:
error01-sol:
error02:
error02-sol:
error03:
error03-sol:
protocol01:

rust-git:
	rm -f rustc cargo rust rustpkg
	ln -s /home/daku/daniel/local/rust/x86_64-unknown-linux-gnu/stage2/bin/rustc .
	ln -s /home/daku/daniel/local/rust/x86_64-unknown-linux-gnu/stage2/bin/cargo .

rust-0.5:
	rm -f rustc cargo rust rustpkg
	ln -s /home/daku/daniel/local/rust-0.5/x86_64-unknown-linux-gnu/stage2/bin/rustc .
	ln -s /home/daku/daniel/local/rust-0.5/x86_64-unknown-linux-gnu/stage2/bin/cargo .

rust-0.6:
	rm -f rustc cargo rust rustpkg
	ln -s /home/daku/daniel/local/rust-0.6/x86_64-unknown-linux-gnu/stage2/bin/rustc .
	ln -s /home/daku/daniel/local/rust-0.6/x86_64-unknown-linux-gnu/stage2/bin/rust .
	ln -s /home/daku/daniel/local/rust-0.6/x86_64-unknown-linux-gnu/stage2/bin/rustpkg .

%: %.rs
	./rustc -L . "$<"

liblazy_list.so: lazy_list.rs
	rm -f liblazy_list-*.so
	./rustc --lib lazy_list.rs

lazy_list: lazy_list.rs
	./rustc --test lazy_list.rs && ./lazy_list

liblazy_list2.so: lazy_list2.rs
	rm -f liblazy_list2-*.so
	./rustc --lib lazy_list2.rs

lazy_list3: lazy_list3.rs
	./rustc --test lazy_list3.rs && ./lazy_list3

liblazy_list3.so: lazy_list3.rs
	rm -f liblazy_list3-*.so
	./rustc --lib lazy_list3.rs

lazy_list2: lazy_list2.rs
	./rustc --test lazy_list2.rs && ./lazy_list2

liblinked_list.so: linked_list.rs
	rm -f liblinked_list-*.so
	./rustc --lib linked_list.rs

linked_list: linked_list.rs
	./rustc --test linked_list.rs && ./linked_list

uses_lazy_list: liblazy_list.so lazy_list.rs
	./rustc -L . uses_lazy_list.rs

libvec_util.so: vec_util.rs
	./rustc --lib vec_util.rs

vec_util: vec_util.rs
	./rustc --test vec_util.rs && ./vec_util

hackvm: hackvm.rs
	./rustc hackvm.rs 

