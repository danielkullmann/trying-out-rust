/**
 * Instruction  Description:
 * ' '  Do Nothing
 * 'p'  Print S0 interpreted as an integer
 * 'P'  Print S0 interpreted as an ASCII character (only the least significant 7 bits of the value are used)
 * '0'  Push the value 0 on the stack
 * '1'  Push the value 1 on the stack
 * '2'  Push the value 2 on the stack
 * '3'  Push the value 3 on the stack
 * '4'  Push the value 4 on the stack
 * '5'  Push the value 5 on the stack
 * '6'  Push the value 6 on the stack
 * '7'  Push the value 7 on the stack
 * '8'  Push the value 8 on the stack
 * '9'  Push the value 9 on the stack
 * '+'  Push S1+S0
 * '-'  Push S1-S0
 * '*'  Push S1*S0
 * '/'  Push S1/S0
 * ':'  Push -1 if S1<S0, 0 if S1=S0, or 1 S1>S0
 * 'g'  Add S0 to the program counter
 * '?'  Add S0 to the program counter if S1 is 0
 * 'c'  Push the program counter on the call stack and set the program counter to S0
 * '$'  Set the program counter to the value pop'ed from the call stack
 * '<'  Push the value of memory cell S0
 * '>'  Store S1 into memory cell S0
 * '^'  Push a copy of S<S0+1> (ex: 0^ duplicates S0)
 * 'v'  Remove S<S0+1> from the stack and push it on top (ex: 1v swaps S0 and S1)
 * 'd'  Drop S0
 * '!'  Terminate the program
 */

enum Command{
  Nop,
  PrintInt,
  PrintChar,
  Push(int),
  Plus,
  Minus,
  Mult,
  Divn,
  Sign,
  Jump,
  JumpIfZero,
  SavePC,
  RestorePC,
  Write,
  Read,
  CopyV,
  MoveV,
  Drop,
  Quit
}

static MEM_SIZE : uint = 128u;
static STACK_SIZE : uint = 256u;

fn parse(programStr: ~str) -> ~[Command] {
  //let mut program : ~[Command] =  ~[];
  let program = vec::map(str::to_chars(programStr), |ch: &char| {
    match *ch {
      ' ' => Nop,     'p' => PrintInt,   'P' => PrintChar,
      '0' => Push(0), '1' => Push(1),    '2' => Push(2), '3' => Push(3),   '4' => Push(4),
      '5' => Push(5), '6' => Push(6),    '7' => Push(7), '8' => Push(8),   '9' => Push(9),
      '+' => Plus,    '-' => Minus,      '*' => Mult,    '/' => Divn,      ':' => Sign,
      'g' => Jump,    '?' => JumpIfZero, 'c' => SavePC,  '$' => RestorePC, '<' => Read,
      '>' => Write,   '^' => CopyV,      'v' => MoveV,   'd' => Drop,      '!' => Quit,
      _ => {fail!(fmt!("Unrecognized command: %c", *ch))}
    }
  });
  return program;
}
  

fn execute(programStr: ~str, mut memory: ~[int]) {
  let program : ~[Command] = parse(programStr);
  let mut stack : ~[int] = ~[];
  let mut call_stack : ~[int] = ~[];
  let mut pc = 0;
  loop {
    let mut incPc = true;
    if pc < 0 {
      io::println(fmt!("ERROR: pc too small %i", pc));
      return;
    }
    if pc >= program.len() as int {
      io::println(fmt!("ERROR: pc too large %i", pc));
      return;
    }
    match program[pc] {
      Nop        => {},
      PrintInt   => {io::println(fmt!("%i",stack.pop()))},
      PrintChar  => {io::println(fmt!("%c",stack.pop() as char))},
      Push(n)    => {stack.push(n)},
      Plus       => {let a = stack.pop(); let b = stack.pop(); stack.push(a + b); },
      Minus      => {let a = stack.pop(); let b = stack.pop(); stack.push(b - a); },
      Mult       => {let a = stack.pop(); let b = stack.pop(); stack.push(a * b); },
      Divn       => {let a = stack.pop(); let b = stack.pop(); stack.push(b / a); },
      Sign       => {let a = stack.pop(); let b = stack.pop(); match b-a {
                      n if n < 0 => stack.push(-1),
                      n if n > 0 => stack.push(1),
                      _          => stack.push(0)
                    }},
      Jump       => {let a = stack.pop(); pc += a; incPc = false; },
      JumpIfZero => {let a = stack.pop(); let b = stack.pop(); if b == 0 { pc += a; incPc = false; } },
      SavePC     => {let a = stack.pop(); call_stack.push(pc+1); pc = a; incPc = false; },
      RestorePC  => {let a = call_stack.pop(); pc = a; incPc = false; },
      Read       => {let a = stack.pop(); stack.push(memory[a]); },
      Write      => {let a = stack.pop(); let b = stack.pop(); memory[a] = b; },
      CopyV      => {let a = stack.pop(); let b = stack[stack.len()-1-a as uint]; stack.push(b); },
      MoveV      => {
                      let a = stack.pop();
                      let sl = stack.len();
                      let v = stack[sl-a as uint];
                      let stack2 : ~[int] = stack;
                      let b = vec::slice(stack2, 0, sl-a as uint);
                      let c = vec::slice(stack2, sl+1-a as uint, sl);
                      stack = ~[] + b + c + ~[v];
                    },
      Drop       => {stack.pop(); },
      Quit       => {return},
    }
    if incPc { pc += 1; }
  }
}


fn main() {
  io::println("== Hack VM ==");
  let mut mem : ~[int] = ~[]; 
  // TODO Find a better way to create a fixed-size vector
  for uint::range(0,MEM_SIZE) |_| {
    mem.push(0);
  };
  let args = os::args();
  if args.len() < 2 {
    io::println("Usage: hackvm program [memory]");
    io::println("e.g. hackvm '0<1<2<3<+++p!' '1,2,4,8'");
    return;
  }
  if args.len() > 2 {
    let mut i = 0u;
    for str::each_split_char(args[2], ',') |s| {
      match int::from_str(s) {
        Some(n) => mem[i] = n,
        _ => fail!(~"memory argument must be integers separated by commas")
      }
      i += 1;
    }
  }
  execute(copy args[1], mem);
  //io::println("== memory ==");
}

