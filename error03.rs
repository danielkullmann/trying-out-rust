fn f(s: ~str) {
  io::println(s);
}

fn main() {
  f(os::args()[0]);
}
