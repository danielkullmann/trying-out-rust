#[link(name = "vec_util", vers = "0.1", author = "Daniel Kullmann")];
#[crate_type = "lib"];

use core::vec::{with_capacity, len, from_slice};

pub fn count<T: Sub<T,T>+Div<T,T>+Add<T,T>+Ord+NumCast+Copy>(start: T, end: T, step: T) -> ~[T] {
  let length = (end-start/step).to_int() as uint;
  let mut vec: ~[T] = with_capacity(length);
  let mut value = start;
  while value <= end {
    vec.push(copy value);
    value += step;
  }
  return vec;
}

pub fn iterate<T: Copy>(start: T, f: &fn(T) -> T, length: uint) -> ~[T] {
  let mut vec: ~[T] = with_capacity(length);
  let mut value = start;
  for uint::range(0, length) |_i| {
    vec.push(copy value);
    value = f(value);
  }
  return vec;
}

pub fn repeat<T: Copy>(value: T, length: uint) -> ~[T] {
  let mut vec: ~[T] = with_capacity(length);
  for uint::range(0, length) |_i| {
    vec.push(copy value);
  }
  return vec;
}

pub fn cycle<T: Copy>(values: ~[T], length: uint) -> ~[T] {
  let mut vec: ~[T] = with_capacity(length);
  for uint::range(0, length) |i| {
    vec.push(copy values[i % len(values)]);
  }
  return vec;
}

pub fn take<T: Copy>(values: ~[T], length: uint) -> ~[T] {
  let mut vec: ~[T] = with_capacity(length);
  for uint::range(0, length) |i| {
    vec.push(copy values[i]);
  }
  return vec;
}

pub fn takewhile<T: Copy>(values: ~[T], f: &fn(&T) -> bool) -> ~[T] {
  let max = values.len();
  let mut index = 0u;
  while (index < max && f(&values[index])) {
    index += 1;
  }
  return from_slice(values.slice(0,index));
}

pub fn dropwhile<T: Copy>(values: ~[T], f: &fn(&T) -> bool) -> ~[T] {
  let max = values.len();
  let mut index = 0u;
  while (index < max && f(&values[index])) {
    index += 1;
  }
  return from_slice(values.slice(index, max));
}

//fn groupby<T: Copy, U>(values: ~[T], f: &fn(&T) -> U) -> HashMap<U, ~[T]> {
//}

pub fn filter<T: Copy>(values: ~[T], f: &fn(&T) -> bool) -> ~[T] {
  let mut vec: ~[T] = ~[];
  let max = values.len();
  let mut index = 0u;
  while (index < max) {
    if f(&values[index]) {
      vec.push(copy values[index]);
    }
    index += 1;
  }
  return vec;
}

pub fn filternot<T: Copy>(values: ~[T], f: &fn(&T) -> bool) -> ~[T] {
  let mut vec: ~[T] = ~[];
  let max = values.len();
  let mut index = 0u;
  while (index < max) {
    if ! f(&values[index]) {
      vec.push(copy values[index]);
    }
    index += 1;
  }
  return vec;
}

pub fn map<T, U>(values: ~[T], f: &fn(&T) -> U) -> ~[U] {
  let max = values.len();
  let mut vec: ~[U] = with_capacity(max);
  let mut index = 0u;
  while (index < max) {
    vec.push(f(&values[index]));
    index += 1;
  }
  return vec;
}


pub fn map2<T1, T2, U>(v1: ~[T1], v2: ~[T2], f: &fn(&T1, &T2) -> U) -> ~[U] {
  let max = v1.len();
  assert!(max == v2.len());
  let mut vec: ~[U] = with_capacity(max);
  let mut index = 0u;
  while (index < max) {
    vec.push(f(&v1[index], &v2[index]));
    index += 1;
  }
  return vec;
}


#[test]
fn test_count() {
  assert!(count(1,10,1) == ~[1,2,3,4,5,6,7,8,9,10]);
  assert!(count(1,10,2) == ~[1,3,5,7,9]);
  assert!(count(5,10,20) == ~[5]);
  assert!(count(10,5,20) == ~[]);
}

#[test]
fn test_iterate() {

  let double = |x: int| 2*x;

  assert!(iterate(1,double,1) == ~[1]);
  assert!(iterate(1,double,10) == ~[1, 2, 4, 8, 16, 32, 64, 128, 256, 512]);
  assert!(iterate("A",core::util::id,4) == ~["A", "A", "A", "A"]);
}

#[test]
fn test_repeat() {
  assert!(repeat(1,1) == ~[1]);
  assert!(repeat(3,10) == ~[3, 3, 3, 3, 3, 3, 3, 3, 3, 3]);
  assert!(repeat("ABC",3) == ~["ABC", "ABC", "ABC"]);
}

#[test]
fn test_cycle() {
  assert!(cycle(~[1,2,3], 7) == ~[1,2,3,1,2,3,1]);
  assert!(cycle(~["A","B","C"], 4) == ~["A", "B", "C", "A"]);
}


#[test]
fn test_take() {
  assert!(take(~[1,2,3], 2) == ~[1,2]);
}

#[test]
fn test_takewhile() {
  assert!(takewhile(~[1,2,3,4,5,6,7,8,9], |&t| t % 4 != 0) == ~[1,2,3]);
  assert!(takewhile(~[1,2,3,4,5,6,7,8,9], |&t| t < 8) == ~[1,2,3,4,5,6,7]);
}

#[test]
fn test_dropwhile() {
  assert!(dropwhile(~[1,2,3,4,5,6,7,8,9], |&t| t % 4 != 0) == ~[4,5,6,7,8,9]);
  assert!(dropwhile(~[1,2,3,4,5,6,7,8,9], |&t| t < 8) == ~[8,9]);
}

#[test]
fn test_filter() {
  let f1 = |x: &int| *x % 2 == 0;
  let f2 = |x: &int| *x > 5;
  assert!(filter(~[1,2,3,4,5,6,7,8,9], f1) == ~[2,4,6,8]);
  assert!(filter(~[1,2,3,4,5,6,7,8,9], f2) == ~[6,7,8,9]);
}

#[test]
fn test_filternot() {
  let f1 = |x: &int| *x % 2 == 0;
  let f2 = |x: &int| *x > 5;
  assert!(filternot(~[1,2,3,4,5,6,7,8,9], f1) == ~[1,3,5,7,9]);
  assert!(filternot(~[1,2,3,4,5,6,7,8,9], f2) == ~[1,2,3,4,5]);
}

#[test]
fn test_map() {
  let f1 = |x: &int| *x + 2;
  let f2 = |x: &int| *x > 5;
  assert!(map(~[1,2,3,4,5,6,7,8,9], f1) == ~[3,4,5,6,7,8,9,10,11]);
  assert!(map(~[1,2,3,4,5,6,7,8,9], f2) == ~[false,false,false,false,false,true,true,true,true]);
}

#[test]
fn test_map2() {
  let f = |x: &int, y: &int| *x + *y;
  assert!(map2(~[1,2,3,4,5,6,7,8,9,2], ~[9,8,7,6,5,4,3,2,1,2], f) == ~[10,10,10,10,10,10,10,10,10,4]);
}
